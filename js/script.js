$(document).ready(function() {
    $('.fancybox').fancybox();
    $(".various").fancybox({
		maxWidth	: 1280,
		maxHeight	: 960,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
    });
});
	