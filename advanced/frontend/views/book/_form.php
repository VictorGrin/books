<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <label>Дата выхода книги</label><br /> <?= DatePicker::widget([
                'model' => $model,
                'attribute' => 'date',
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
            ]); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?= Html::activeDropDownList($model, 'author_id', $authorList, [
                'prompt' => 'Автор',
                'class' => 'form-control'
            ]) ?>
        </div>
    </div>
    
    <br />
    <div class="form-group">
        <?= $form->field($model, 'preview')->fileInput() ?>
        
        <?= is_file('uploads/book_'.$model->id.'_100.jpg') ? Html::img('/uploads/book_'.$model->id.'_100.jpg') : ''; ?>
    </div>
    
    <?= $form->errorSummary($model); ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
