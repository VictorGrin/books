<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\\models\BookSearch */
/* @var $form yii\widgets\ActiveForm */
if (isset($model->date_from) && $model->date_from) {
    $model->date_from = \DateTime::createFromFormat('d/m/Y', $model->date_from)->format('Y-m-d');
}
if (isset($model->date_till) && $model->date_till) {
    $model->date_till = \DateTime::createFromFormat('d/m/Y', $model->date_till)->format('Y-m-d');
}
?>

<div class="book-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-3">
            <?= Html::activeDropDownList($model, 'author_id', $authorList, [
                'prompt' => 'Автор',
                'class' => 'form-control'
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name', [
                'inputOptions' => [
                    'class'=>'form-control',
                    'placeholder'=>'Название книги'
                ]
            ])->label(false) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-10">
            Дата выхода книги: 
            <?= DatePicker::widget([
                'model' => $model,
                'attribute' => 'date_from',
                'language' => 'ru',
                'dateFormat' => 'dd/MM/yyyy',
            ]); ?>
            до 
            <?= DatePicker::widget([
                'model' => $model,
                'attribute' => 'date_till',
                'language' => 'ru',
                'dateFormat' => 'dd/MM/yyyy',
            ]); ?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
            <?php // echo Html::resetButton('Сбросить', ['class' => 'btn btn-default']) ?>
        </div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
