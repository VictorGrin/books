<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Book */

$this->title = 'Добавление книги';
$this->params['breadcrumbs'][] = ['label' => 'Книги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr />

    <?= $this->render('_form', [
        'model' => $model,
        'authorList' => $authorList,
    ]) ?>

</div>
