<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить автора', ['/author/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <hr />
    
    <?php echo $this->render('_search', ['model' => $searchModel, 'authorList' => $authorList]); ?>
    
    <hr />
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'preview',
                'value' => function ($data) {
                    if (is_file($data->preview)) {
                        return '<a class="fancybox" href="/'.$data->preview.'?date='.$data->updated_at.'" data-fancybox-group="gallery" title=" '.$data->name.'
Автор: '.$data->author[0]->firstname.' '.$data->author[0]->lastname.'  ">
				<img  src="/uploads/book_'.$data->id.'_100.jpg?date='.$data->updated_at.'" alt="" />
			</a>';
                    }
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'author_id',
                'label' => 'Автор',
                'value' => function ($data) {
                    return $data->author[0]->firstname.' '.$data->author[0]->lastname;
                },
                'format' => 'raw'
            ],
            'date:date',
            'created_at:relativetime',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn', 'template'=>'{update}{view}{delete}',
                'header' => 'Кнопки действий',
                'buttons' => [
                    'update' => function ($url, $model) {     
                        return Html::a('[ред.]', $url, [
                            'title' => 'Редактировать',
                            'target' => '_blank',
                        ]);                                
                    },
                    'view' => function ($url, $model) {     
                        return Html::a('[просм.]', $url, [
                            'title' => 'Просмотр '.$model->name,
                            'class' => 'various fancybox.ajax'
                        ]);                                
                    },
                    'delete' => function ($url, $model) {     
                        return Html::a('[удал.]', $url, [
                            'title' => 'Удалить '.$model->name,
                            'data-method' => 'post',
                            'data-confirm' => 'Вы уверены, что хотите удалить эту книгу?'
                        ]);                                
                    },
                ]
            ],
        ],
    ]); ?>

</div>
