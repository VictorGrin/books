<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Book;
use frontend\models\BookSearch;
use frontend\models\Author;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember();
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 5;
        
        $authorList = ArrayHelper::map(
            Author::find()->orderBy(['firstname' => SORT_ASC,])->all(),
            'id', function($model, $defaultValue) {
                return $model->firstname.' '.$model->lastname;
            }
        );
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'authorList' => $authorList,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $authorList = ArrayHelper::map(
            Author::find()->orderBy(['firstname' => SORT_ASC,])->all(),
            'id', function($author, $defaultValue) {
                return $author->firstname.' '.$author->lastname;
            }
        );
        
        $model = new Book();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->preview = UploadedFile::getInstance($model, 'preview');
            if ($model->upload()) {
                $model->save();
            }
            \Yii::$app->getSession()->setFlash('success', 'Книга успешно добавлена в библиотеку.');
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'authorList' => $authorList,
            ]);
        }
    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $authorList = ArrayHelper::map(
            Author::find()->orderBy(['firstname' => SORT_ASC,])->all(),
            'id', function($author, $defaultValue) {
                return $author->firstname.' '.$author->lastname;
            }
        );
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->preview = UploadedFile::getInstance($model, 'preview');
            if ($model->upload()) {
                $model->save();
            }
            \Yii::$app->getSession()->setFlash('success', 'Книга успешно отредактирована.');
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect([Url::previous()]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'authorList' => $authorList,
            ]);
        }
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if(is_file($model->preview)) {
            unlink($model->preview);
        }
        if(is_file('uploads/book_'.$model->id.'_100.jpg')) {
            unlink('uploads/book_'.$model->id.'_100.jpg');
        }
        
        if ($model->delete()) {
            \Yii::$app->getSession()->setFlash('success', 'Книга успешно удалена.');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
