<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * This is the model class for table "book".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $preview
 * @property string $date
 * @property integer $author_id
 */
class Book extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date', 'author_id'], 'required'],
            [['created_at', 'updated_at', 'author_id'], 'integer'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['preview'], 'safe'],
            [['preview'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg']
        ];
    }

    public function attributes() {
        $attributes = parent::attributes();
        return array_merge($attributes, [
            'date_from',
            'date_till'
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'created_at' => 'Дата добавления',
            'updated_at' => 'Дата редактирования',
            'preview' => 'Превью',
            'date' => 'Дата выхода книги',
            'author_id' => 'Автор',
        ];
    }
    
    public function getAuthor()
    {
        return $this->hasMany(Author::className(), ['id' => 'author_id']);
    }
    
    public function upload()
    {
        if ($this->validate()) {
            //$this->preview->saveAs('uploads/' . $this->preview->baseName . '.' . $this->preview->extension);
            $filename = 'uploads/book_'.$this->id.'.jpg';
            $thumb = 'uploads/book_'.$this->id.'_100.jpg';
                //Yii::$app->db->createCommand("UPDATE user SET `avatar`='$filename' WHERE id=:id") ->bindValue(':id', $userID) ->execute();
                //$filename = 'uploads/avatar/'.$dir.'/user_'.$userID.'_avatar_' . Yii::$app->security->generateRandomString() . '.jpg';
                if (isset($this->preview->tempName)) {
                    Image::thumbnail($this->preview->tempName, 800, 600)->save($filename, ['quality' => 100]);
                    Image::thumbnail($this->preview->tempName, 100, 70)->save($thumb, ['quality' => 100]);
                }
            $this->preview = isset($this->preview->tempName) ? $filename : null;
            return true;
        } else {
            $this->preview = null;
            return false;
        }
    }
}
